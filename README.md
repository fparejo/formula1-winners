# Formula 1 Winners

Small project to show forrmula 1 winners from 2008 to 2018

## Run Project

With npm:

Install dependencies:

```
npm i
```
Run development environment:

```
npm run dev
```

Run production environment:

```
npm run start
```

Run test environment:

```
npm run test
```

Run flow checking script:

```
npm run flow
```

## Why did I use Redux?
I think that for this project it is not necessary to use Redux library because it is useful for medium/big projects.

For small projects it adds an extra complexity.

It can be avoided by using React Context or smaller libraries like Flux.  

I used Redux because I wanted to show that I feel confortable with it.

## Isolate webpack environment configurations in different files.
In my opinion, having different webpack configuration files facilitates maintainability and readability.

And thanks to webpack-merge library it is easy to implement.

I created three configuration files: 
  - webpack.config.js: common webpack configuration.
  - webpack.development.js: development webpack configuration.
  - webpack.production.js: production webpack configuration.  

## Domain-style architecture
This a small project and I could apply a rails-style architecture (actions, reducers, selectors in separate folders).

But I started to develop the project based on features, so for me it was easier to separate them in different folders.

## Normalizr
I've never used Normalizr library but I read about it before working on this project.

I found it interesting for modelling the data received from driversStanding API.

Normilizr allowed me to create a data structure adequate for driver standings.

## Styled Components
I like the CSS-in-JS concept because abstracts the CSS model to the component level and it makes the styles more modular.

I have some experience with Styled Components library so that's why I implement all the styles with it.

But there are more alternatives like Emotion or Glamorous.

## Authors

* [Frank Parejo](https://github.com/frankPairs)
