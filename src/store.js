import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import middlewares from './middlewares';
import { seasonsReducer, SEASONS_MODULE_NAME } from './modules/seasons';
import { driversReducer, DRIVERS_MODULE_NAME } from './modules/drivers';

const reducer = combineReducers({
  [SEASONS_MODULE_NAME]: seasonsReducer,
  [DRIVERS_MODULE_NAME]: driversReducer
});
// eslint-disable-next-line
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  {},
  composeEnhancers(applyMiddleware(...middlewares))
);

export default store;
