// @flow
import React from 'react';
import type { Node } from 'react';
import { Wrapper } from './styles';

function ArrowRight(): Node {
  return <Wrapper />;
}

export default ArrowRight;
