import styled from 'styled-components';

export const Wrapper = styled.i`
  border: solid ${props => props.theme.colors.gray};
  border-width: 0 1px 1px 0;
  display: inline-block;
  height: 3px;
  padding: 3px;
  transform: rotate(-45deg);
  width: 3px;
`;
