// @flow
import React from 'react';
import type { Node } from 'react';
import SpinnerIcon from 'assets/svg/spinner2.svg';
import { Wrapper } from './styles';

function CircularProgress(): Node {
  return (
    <Wrapper>
      <SpinnerIcon />
    </Wrapper>
  );
}

export default CircularProgress;
