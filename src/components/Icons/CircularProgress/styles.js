import styled, { keyframes } from 'styled-components';

const spinner = keyframes`
  to {transform: rotate(360deg);}
`;

export const Wrapper = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  justify-content: center;
  position: relative;
  width: 100%;

  svg {
    animation: ${spinner} 0.6s linear infinite;
  }
`;
