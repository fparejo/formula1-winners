// @flow
import React from 'react';
import type { Node } from 'react';
import Icon from 'assets/svg/heart.svg';

type Props = {
  height?: number,
  width?: number
};

function FavoriteIcon({ height, width }: Props): Node {
  return <Icon height={height} width={width} />;
}

FavoriteIcon.defaultProps = {
  width: 20,
  height: 20
};
export default FavoriteIcon;
