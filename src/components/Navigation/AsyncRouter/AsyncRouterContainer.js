// @flow
import React from 'react';
import type { Node } from 'react';

type Props = {
  page: Promise<*>
};

type State = {
  Component: Node
};

class AsyncRouter extends React.Component<Props, State> {
  state = {
    Component: null
  };

  componentDidMount(): void {
    const { page } = this.props;

    console.log(page);
    page.then(this.handleLoadComponent);
  }

  handleLoadComponent = (importResult: { default: Node }): void => {
    this.setState({ Component: importResult.default });
  };

  render(): Node {
    const { Component } = this.state;

    return !Component ? null : <Component {...this.props} />;
  }
}

export default AsyncRouter;
