import styled from 'styled-components';

export const Wrapper = styled.div`
  h2 {
    font-size: ${props => props.theme.fonts.title};
    font-weight: 400;
    text-align: center;
  }

  @media (min-width: ${props => props.theme.device.phone}px) {
    h2 {
      text-align: left;
    }
  }
`;
