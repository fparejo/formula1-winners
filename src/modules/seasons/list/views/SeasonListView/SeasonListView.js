// @flow
import React from 'react';
import type { Node } from 'react';
import { SeasonList } from '../../components';
import { Wrapper } from './styles';

function SeasonListView(): Node {
  return (
    <Wrapper>
      <h2> Latest Seasons </h2>
      <SeasonList />
    </Wrapper>
  );
}

export default SeasonListView;
