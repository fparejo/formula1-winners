// @flow
import get from 'lodash/get';
import seasonsApi from '../api';
import { actions } from './duck';

export const getFilteredSeasons = (from: number = 2008, to: number = 2018) => (
  dispatch: Function
) => {
  function filterBySeason(season) {
    const seasonNumber = Number(season.season);
    return seasonNumber >= from && seasonNumber <= to;
  }

  dispatch(actions.fetchSeasonsStart());

  return seasonsApi
    .getSeasons()
    .then(response => {
      const seasons = get(response, [
        'data',
        'MRData',
        'SeasonTable',
        'Seasons'
      ]);

      dispatch(actions.fetchSeasonsSuccess(seasons.filter(filterBySeason)));
    })
    .catch(err => dispatch(actions.fetchSeasonsFail(err.message)));
};
