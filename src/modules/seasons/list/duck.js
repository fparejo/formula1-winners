// @flow
import { MODULE_NAME } from '../constants';

type State = {
  data: Array<Season>,
  loading: boolean,
  error: ?string
};

export const LIST_STATE_NAME = 'list';

/* ACTIONS */
const actionTypes = {
  SEASONS_FETCH_START: 'SEASONS_FETCH_START',
  SEASONS_FETCH_SUCCESS: 'SEASONS_FETCH_SUCCESS',
  SEASONS_FETCH_FAIL: 'SEASONS_FETCH_FAIL'
};

export const actions = {
  fetchSeasonsStart: (): Action => ({ type: actionTypes.SEASONS_FETCH_START }),
  fetchSeasonsSuccess: (seasons: Array<Season>): Action => ({
    type: actionTypes.SEASONS_FETCH_SUCCESS,
    payload: seasons
  }),
  fetchSeasonsFail: (error: string): Action => ({
    type: actionTypes.SEASONS_FETCH_FAIL,
    payload: error
  })
};

/* REDUCER */
export const initialState: State = {
  data: [],
  loading: false,
  error: null
};

export default function reducer(
  state: State = initialState,
  action: Action
): State {
  switch (action.type) {
    case actionTypes.SEASONS_FETCH_START:
      return { ...state, loading: true };
    case actionTypes.SEASONS_FETCH_SUCCESS:
      return { ...state, loading: false, data: action.payload };
    case actionTypes.SEASONS_FETCH_FAIL:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
}

/* SELECTORS */
export const selectors = {
  selectSeasonsData: (state: Object): Array<Season> =>
    state[MODULE_NAME][LIST_STATE_NAME].data,
  selectSeasonsLoading: (state: Object): boolean =>
    state[MODULE_NAME][LIST_STATE_NAME].loading,
  selectSeasonsError: (state: Object): boolean =>
    state[MODULE_NAME][LIST_STATE_NAME].error
};
