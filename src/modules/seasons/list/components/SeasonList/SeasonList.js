// @flow
import React from 'react';
import type { Node } from 'react';
import { CircularProgress } from 'components/Icons';
import SeasonListItem from '../SeasonListItem/SeasonListItem';
import { Wrapper } from './styles';

type Props = {
  seasons: Array<Season>,
  isLoading: boolean
};

function SeasonList({ seasons, isLoading }: Props): Node {
  if (isLoading) {
    return <CircularProgress />;
  }
  return (
    <Wrapper>
      {seasons.map(season => (
        <SeasonListItem key={season.season} season={season} />
      ))}
    </Wrapper>
  );
}

export default SeasonList;
