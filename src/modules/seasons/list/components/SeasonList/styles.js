import styled from 'styled-components';

export const Wrapper = styled.ul`
  display: block;
  list-style: none;
  padding: 10px 0;
`;
