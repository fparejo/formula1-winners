// @flow
import React, { Component } from 'react';
import type { Node } from 'react';
import { connect } from 'react-redux';
import { getFilteredSeasons } from '../../asyncActions';
import { selectors } from '../../duck';
import SeasonList from './SeasonList';

type MapDispatchToProps = {
  getSeasons: Function
};

type MapStateToProps = {
  seasons: Array<Season>,
  isLoading: boolean
};

type Props = MapDispatchToProps & MapStateToProps;

export class InnerComponent extends Component<Props> {
  componentDidMount(): void {
    const { getSeasons, seasons } = this.props;

    if (seasons.length === 0) {
      getSeasons();
    }
  }

  render(): Node {
    return <SeasonList {...this.props} />;
  }
}

const mapDispatchToProps: MapDispatchToProps = {
  getSeasons: getFilteredSeasons
};

function mapStateToProps(state: Object) {
  return {
    seasons: selectors.selectSeasonsData(state),
    isLoading: selectors.selectSeasonsLoading(state)
  };
}

const SeasonsListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(InnerComponent);

SeasonsListContainer.displayName = 'SeasonsListContainer';

export default SeasonsListContainer;
