import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Wrapper = styled(Link)`
  background-color: ${props => props.theme.colors.white};
  cursor: pointer;
  display: block;
  margin: 0;
  padding: 20px 30px;
  text-decoration: none;

  :hover {
    background-color: ${props => props.theme.colors.background};
  }

  li {
    align-items: center;
    display: flex;
    justify-content: space-between;
  }
`;

export const Title = styled.h3`
  color: black;
  font-size: ${props => props.theme.fonts.big};
  font-weight: 300;
`;
