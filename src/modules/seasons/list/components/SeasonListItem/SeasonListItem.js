// @flow
import React from 'react';
import type { Node } from 'react';
import { ArrowRight } from 'components/Icons';
import { DRIVERS_ROUTES } from 'modules/drivers';
import { Wrapper, Title } from './styles';

type Props = {
  season: Season
};

function SeasonListItem({ season }: Props): Node {
  return (
    <Wrapper
      to={`${DRIVERS_ROUTES.paths.driversStandingsList.staticPath(
        season.season
      )}`}
    >
      <li key={season.season}>
        <Title>{`Season ${season.season}`}</Title>
        <ArrowRight />
      </li>
    </Wrapper>
  );
}

export default SeasonListItem;
