// @flow
import { combineReducers } from 'redux';
import listReducer, { LIST_STATE_NAME } from './list/duck';

export default combineReducers({
  [LIST_STATE_NAME]: listReducer
});
