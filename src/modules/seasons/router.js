// @flow
import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import { SeasonListView } from './list/views';
import { ROUTES } from './constants';

function SeasonsRouter({ match }: Route): React.Node {
  return (
    <Switch>
      <Route
        exact
        path={`${match.path}${ROUTES.paths.seasonList.path}`}
        component={SeasonListView}
      />
    </Switch>
  );
}

export default SeasonsRouter;
