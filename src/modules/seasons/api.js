// @flow
import api from 'api';

function getSeasons(limit: number = 70, offset: number = 0): Promise<*> {
  return api.get(`/seasons.json?limit=${limit}&offset=${offset}`);
}

export default {
  getSeasons
};
