export const MODULE_NAME = 'seasons';

export const ROUTE_PATH = '/seasons';

export const ROUTES = {
  paths: {
    seasonList: {
      path: '/'
    },
    seasonDetail: {
      path: '/:season'
    }
  }
};
