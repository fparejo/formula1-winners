const theme = {
  device: {
    desktop: 992,
    tablet: 768,
    phone: 576
  },
  colors: {
    background: '#f7f4f1',
    red: '#e10600',
    white: '#fff',
    black: '#000',
    gray: 'rgba(0, 0, 0, 0.54)'
  },
  fonts: {
    verySmall: '0.75rem',
    small: '0.875rem',
    medium: '1rem',
    big: '1.25rem',
    veryBig: '1.5rem',
    title: '2.25rem'
  },
  fontsWeights: {
    light: '300',
    regular: '400',
    bold: '700'
  }
};

export default theme;
