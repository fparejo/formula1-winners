// @flow
import React, { Fragment } from 'react';
import type { Node } from 'react';
import { Link } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { SEASONS_ROUTE_PATH } from 'modules/seasons';
import theme from './theme';
import { Main, Content, Header, GlobalStyle } from './styles';

type Props = {
  children: Node
};

function AppLayout({ children }: Props): Node {
  return (
    <ThemeProvider theme={theme}>
      <Fragment>
        <GlobalStyle />
        <Header>
          <Link to={SEASONS_ROUTE_PATH}>
            <img
              className="logo"
              src="https://www.formula1.com/etc/designs/fom-website/images/f1_logo.svg"
              alt="F1 Logo"
            />
          </Link>
        </Header>
        <Main>
          <Content>{children}</Content>
        </Main>
      </Fragment>
    </ThemeProvider>
  );
}

export default AppLayout;
