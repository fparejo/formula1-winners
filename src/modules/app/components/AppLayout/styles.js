import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  html {
    min-height: 100vh;
  }

  body, html, #app {
    font-family: "Titillium Web";
    font-size: 16px;
    height: 100%;
    margin: 0;
  }

  body {
    background-color: ${props => props.theme.colors.background};
  }

  h1, h2, h3, h4, p, span, ul, li {
    margin: 0;
    padding: 0;
  }
`;

export const Main = styled.main`
  display: grid;
  grid-template-columns: 100%;
  padding: 20px;

  @media (min-width: ${props => props.theme.device.phone}px) {
    grid-template-columns: 100px auto 100px;
  }
`;

export const Header = styled.header`
  align-items: center;
  background-color: ${props => props.theme.colors.red};
  display: flex;
  height: 60px;
  justify-content: center;
  padding: 20px;

  .logo {
    height: auto;
    width: 120px;
  }

  @media (min-width: ${props => props.theme.device.phone}px) {
    height: 80px;
    justify-content: flex-start;

    .logo {
      height: auto;
      width: 140px;
    }
  }
`;

export const Content = styled.div`
  grid-column-start: 1;
  width: 100%;

  @media (min-width: ${props => props.theme.device.phone}px) {
    grid-column-start: 2;
  }
`;
