// @flow
import React from 'react';
import type { Node } from 'react';
import { hot } from 'react-hot-loader';
import { Route, Switch, Redirect } from 'react-router-dom';
import { AsyncRouter } from 'components/Navigation';
import { SEASONS_ROUTE_PATH } from '../seasons';
import { DRIVERS_ROUTE_PATH } from '../drivers';
import { AppLayout } from './components';

function AppRouter(): Node {
  return (
    <AppLayout>
      <Switch>
        <Route
          path={SEASONS_ROUTE_PATH}
          component={props => (
            <AsyncRouter {...props} page={import('../seasons/router')} />
          )}
        />
        <Route
          path={DRIVERS_ROUTE_PATH}
          component={props => (
            <AsyncRouter {...props} page={import('../drivers/router')} />
          )}
        />
        <Redirect to={SEASONS_ROUTE_PATH} />
      </Switch>
    </AppLayout>
  );
}

export default hot(module)(AppRouter);
