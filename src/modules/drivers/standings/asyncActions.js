// @flow
import get from 'lodash/get';
import { actions as listActions } from '../list/duck';
import driversApi from '../api';
import { actions } from './duck';
import normalizer from '../normalizer';

export const fetchStandingsBySeason = (season: string) => (
  dispatch: Function
) => {
  dispatch(actions.fetchDriversStandingsStart());

  return driversApi
    .getStandingsBySeason(season)
    .then(response => {
      const driversStandingFromAPI = get(response, [
        'data',
        'MRData',
        'StandingsTable',
        'StandingsLists',
        0,
        'DriverStandings'
      ]);

      const {
        entities: { drivers },
        result
      } = normalizer.driversStandings(driversStandingFromAPI);

      dispatch(actions.fetchDriversStandingsSuccess());
      dispatch(listActions.updateDriversData(drivers));
      dispatch(actions.setDriversStandingsBySeason(season, result));
    })
    .catch(err => dispatch(actions.fetchDriversStandingsFail(err.message)));
};
