// @flow
import React from 'react';
import type { Node } from 'react';
import type { Driver, DriverStanding } from 'modules/drivers/types';
import { DriverFavoriteIcon } from 'modules/drivers/favorites/components';
import { Wrapper, Header, Content, Footer } from './styles';

type Props = {
  driver: Driver,
  driverStanding: DriverStanding
};

function DriverStandingItem({ driver, driverStanding }: Props): Node {
  return (
    <Wrapper>
      <Header isFirst={Number(driverStanding.position) === 1}>
        <span className="position">{driverStanding.position}</span>
        <h3>{`${driver.givenName} ${driver.familyName}`}</h3>
      </Header>
      <Content>
        <p>{`Nationality: ${driver.nationality}`}</p>
        <p>{`Birthday: ${driver.dateOfBirth}`}</p>
        <p>{`Constructor: ${driverStanding.Constructors[0]}`}</p>
      </Content>
      <Footer>
        <DriverFavoriteIcon driverId={driver.driverId} />
      </Footer>
    </Wrapper>
  );
}

export default DriverStandingItem;
