import styled from 'styled-components';

export const Wrapper = styled.li`
  background-color: ${props => props.theme.colors.white};
  box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.2),
    0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  margin: 20px 0;
  max-width: 400px;
  width: 100%;

  :first-child {
    margin: 0;
  }

  @media (min-width: ${props => props.theme.device.phone}px) {
    margin: 0 0 20px 20px;

    :first-child {
      margin: 0 0 20px 20px;
    }
  }
`;

export const Header = styled.div`
  align-items: center;
  display: flex;
  padding: 20px 20px 0;

  h3 {
    font-size: ${props => props.theme.fonts.big};
    font-weight: ${props => props.theme.fontsWeights.light};
    margin-left: 10px;
  }

  .position {
    align-items: center;
    background-color: ${props =>
      props.isFirst ? props.theme.colors.red : props.theme.colors.black};
    border-radius: 50px;
    color: ${props => props.theme.colors.white};
    display: flex;
    font-size: ${props => props.theme.fonts.big};
    font-weight: ${props => props.theme.fontsWeights.bold};
    justify-content: center;
    height: 40px;
    width: 40px;
  }
`;

export const Content = styled.div`
  padding: 20px;

  p {
    color: ${props => props.theme.colors.gray};
    font-size: ${props => props.theme.fonts.normal};
  }
`;

export const Footer = styled.div`
  align-items: center;
  display: flex;
  justify-content: flex-start;
  height: 40px;
  padding: 0 20px;
`;
