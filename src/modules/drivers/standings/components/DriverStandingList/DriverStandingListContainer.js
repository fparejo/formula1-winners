// @flow
import React, { Component } from 'react';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import type { Node } from 'react';
import { connect } from 'react-redux';
import { selectors as listSelectors } from 'modules/drivers/list/duck';
import type { DriverStanding, Driver } from 'modules/drivers/types';
import { fetchStandingsBySeason } from '../../asyncActions';
import { selectors } from '../../duck';
import DriverStandingList from './DriverStandingList';

type MapDispatchToProps = {
  fetchStandings: Function
};

type MapStateToProps = {
  driversStandings: Array<DriverStanding>,
  drivers: { [string]: Driver },
  isLoading: boolean
};

type Props = MapDispatchToProps &
  MapStateToProps & {
    match: {
      params: {
        season: string
      }
    }
  };

export class InnerComponent extends Component<Props> {
  componentDidMount(): void {
    const {
      match: { params },
      driversStandings,
      fetchStandings
    } = this.props;

    if (driversStandings.length === 0) {
      fetchStandings(params.season);
    }
  }

  render(): Node {
    return <DriverStandingList {...this.props} />;
  }
}

const mapDispatchToProps: MapDispatchToProps = {
  fetchStandings: fetchStandingsBySeason
};

function mapStateToProps(
  state: Object,
  { match: { params } }: Props
): MapStateToProps {
  return {
    driversStandings: selectors.selectDriversStandingsBySeason(
      state,
      params.season
    ),
    drivers: listSelectors.selectDrivers(state),
    isLoading: selectors.selectDriversStandingsLoading(state)
  };
}

const DriverStandingListContainer = compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(InnerComponent);

DriverStandingListContainer.displayName = 'DriverStandingListContainer';

export default DriverStandingListContainer;
