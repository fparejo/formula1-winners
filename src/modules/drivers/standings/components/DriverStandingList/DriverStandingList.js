// @flow
import React from 'react';
import type { Node } from 'react';
import type { DriverStanding, Driver } from 'modules/drivers/types';
import { CircularProgress } from 'components/Icons';
import DriverStandingItem from '../DriverStandingItem/DriverStandingItem';
import { Wrapper } from './styles';

type Props = {
  driversStandings: Array<DriverStanding>,
  drivers: { [string]: Driver },
  isLoading: boolean
};

function DriverStandingList({
  driversStandings,
  drivers,
  isLoading
}: Props): Node {
  if (isLoading) {
    return <CircularProgress />;
  }

  return (
    <Wrapper>
      {driversStandings.map(driverStanding => (
        <DriverStandingItem
          key={driverStanding.Driver}
          driver={drivers[driverStanding.Driver]}
          driverStanding={driverStanding}
        />
      ))}
    </Wrapper>
  );
}

export default DriverStandingList;
