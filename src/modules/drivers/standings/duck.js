// @flow
import { MODULE_NAME } from '../constants';
import type { DriverStanding } from '../types';

type State = {
  bySeason: { [string]: Array<DriverStanding> },
  loading: boolean,
  error: ?string
};

export const STANDINGS_STATE_NAME = 'standings';

/* ACTIONS */
const actionTypes = {
  FETCH_DRIVERS_STANDINGS_START: 'FETCH_DRIVERS_STANDINGS_START',
  FETCH_DRIVERS_STANDINGS_SUCCESS: 'FETCH_DRIVERS_STANDINGS_SUCCESS',
  FETCH_DRIVERS_STANDINGS_FAIL: 'FETCH_DRIVERS_STANDINGS_FAIL',
  SET_DRIVERS_STANDINGS_BY_SEASON: 'SET_DRIVERS_STANDINGS_BY_SEASON'
};

export const actions = {
  fetchDriversStandingsStart: (): Action => ({
    type: actionTypes.FETCH_DRIVERS_STANDINGS_START
  }),
  fetchDriversStandingsSuccess: (): Action => ({
    type: actionTypes.FETCH_DRIVERS_STANDINGS_SUCCESS
  }),
  fetchDriversStandingsFail: (error: string): Action => ({
    type: actionTypes.FETCH_DRIVERS_STANDINGS_FAIL,
    payload: error
  }),
  setDriversStandingsBySeason: (
    season: string,
    driversStandings: Array<DriverStanding>
  ) => ({
    type: actionTypes.SET_DRIVERS_STANDINGS_BY_SEASON,
    payload: {
      season,
      driversStandings
    }
  })
};

/* REDUCER */
export const initialState: State = {
  bySeason: {},
  loading: false,
  error: null
};

export default function reducer(
  state: State = initialState,
  action: Action
): State {
  switch (action.type) {
    case actionTypes.FETCH_DRIVERS_STANDINGS_START:
      return { ...state, loading: true };
    case actionTypes.FETCH_DRIVERS_STANDINGS_SUCCESS:
      return { ...state, loading: false };
    case actionTypes.FETCH_DRIVERS_STANDINGS_FAIL:
      return { ...state, loading: false, error: action.payload };
    case actionTypes.SET_DRIVERS_STANDINGS_BY_SEASON:
      return {
        ...state,
        bySeason: {
          ...state.bySeason,
          [action.payload.season]: action.payload.driversStandings
        }
      };
    default:
      return state;
  }
}

/* SELECTORS */
export const selectors = {
  selectDriversStandingsBySeason: (
    state: Object,
    season: string
  ): Array<DriverStanding> => {
    const driversStandings =
      state[MODULE_NAME][STANDINGS_STATE_NAME].bySeason[season];
    return Array.isArray(driversStandings) ? driversStandings : [];
  },
  selectDriversStandingsLoading: (state: Object): boolean =>
    state[MODULE_NAME][STANDINGS_STATE_NAME].loading,
  selectDriversStandingsError: (state: Object): string =>
    state[MODULE_NAME][STANDINGS_STATE_NAME].error
};
