// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fecthFavoriteDriversStorage } from 'modules/drivers/favorites/asyncActions';
import { selectors } from 'modules/drivers/favorites/duck';
import DriverStandingListView from './DriverStandingListView';

type MapDispatchToProps = {
  fecthFavorites: Function
};

type MapStateToProps = {
  wasFetched: boolean
};

type Props = MapDispatchToProps &
  MapStateToProps & {
    match: {
      params: {
        season: string
      }
    }
  };

export class InnerComponent extends Component<Props> {
  componentDidMount() {
    const { fecthFavorites, wasFetched } = this.props;

    if (!wasFetched) {
      fecthFavorites();
    }
  }

  render() {
    return <DriverStandingListView {...this.props} />;
  }
}

const mapDispatchToProps: MapDispatchToProps = {
  fecthFavorites: fecthFavoriteDriversStorage
};

function mapStateToProps(state: Object): MapStateToProps {
  return {
    wasFetched: selectors.selectWasFetchedFromStorage(state)
  };
}

const DriverStandingListViewContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(InnerComponent);

DriverStandingListViewContainer.displayName = 'DriverStandingListViewContainer';

export default DriverStandingListViewContainer;
