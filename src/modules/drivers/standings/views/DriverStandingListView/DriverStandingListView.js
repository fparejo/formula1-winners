// @flow
import React from 'react';
import type { Node } from 'react';
import { Wrapper } from './styles';
import { DriverStandingList } from '../../components';

type Props = {
  match: {
    params: {
      season: string
    }
  }
};

function DriverStandingListView({ match }: Props): Node {
  return (
    <Wrapper>
      <h2>{`Driver standings ${match.params.season}`}</h2>
      <DriverStandingList />
    </Wrapper>
  );
}

export default DriverStandingListView;
