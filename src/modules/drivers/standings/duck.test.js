import { MODULE_NAME } from '../constants';
import reducer, {
  actions,
  initialState,
  selectors,
  STANDINGS_STATE_NAME
} from './duck';

describe('Drivers standings duck', () => {
  describe('Actions', () => {
    test('should fetchDriversStandingsStart return an action', () => {
      const action = actions.fetchDriversStandingsStart();
      expect(action).toMatchSnapshot();
    });

    test('should fetchDriversStandingsSuccess return an action', () => {
      const action = actions.fetchDriversStandingsSuccess();
      expect(action).toMatchSnapshot();
    });

    test('should fetchDriversStandingsFail return an action', () => {
      const action = actions.fetchDriversStandingsFail();
      expect(action).toMatchSnapshot();
    });

    test('should setDriversStandingsBySeason return an action', () => {
      const action = actions.setDriversStandingsBySeason('1', ['1', '2']);
      expect(action).toMatchSnapshot();
    });
  });

  describe('Reducer', () => {
    test('should FETCH_DRIVERS_STANDINGS_START return a new state', () => {
      const action = actions.fetchDriversStandingsStart();
      const mockState = reducer(initialState, action);

      expect(mockState).toEqual({ ...initialState, loading: true });
    });

    test('should FETCH_DRIVERS_STANDINGS_SUCCESS return a new state', () => {
      const action = actions.fetchDriversStandingsSuccess();
      const mockState = reducer(initialState, action);

      expect(mockState).toEqual({ ...initialState, loading: false });
    });

    test('should FETCH_DRIVERS_STANDINGS_FAIL return a new state', () => {
      const action = actions.fetchDriversStandingsFail('error');
      const mockState = reducer(initialState, action);

      expect(mockState).toEqual({
        ...initialState,
        loading: false,
        error: 'error'
      });
    });

    test('should SET_DRIVERS_STANDINGS_BY_SEASON return a new state', () => {
      const action = actions.setDriversStandingsBySeason('1', ['1', '2']);
      const mockState = reducer(initialState, action);

      expect(mockState).toEqual({
        ...initialState,
        bySeason: { '1': ['1', '2'] }
      });
    });
  });

  describe('Selectors', () => {
    test('should selectDriversStandingsLoading return loading value', () => {
      const mockState = {
        [MODULE_NAME]: {
          [STANDINGS_STATE_NAME]: { ...initialState, loading: true }
        }
      };
      const result = selectors.selectDriversStandingsLoading(mockState);

      expect(result).toBe(true);
    });

    test('should selectDriversStandingsError return error value', () => {
      const mockState = {
        [MODULE_NAME]: {
          [STANDINGS_STATE_NAME]: { ...initialState, error: 'error' }
        }
      };
      const result = selectors.selectDriversStandingsError(mockState);

      expect(result).toBe('error');
    });

    test('should selectDriversStandingsBySeason return error value', () => {
      const mockState = {
        [MODULE_NAME]: {
          [STANDINGS_STATE_NAME]: {
            ...initialState,
            bySeason: { '1': ['1', '2'] }
          }
        }
      };
      const result = selectors.selectDriversStandingsBySeason(mockState, '1');

      expect(result).toEqual(['1', '2']);
    });
  });
});
