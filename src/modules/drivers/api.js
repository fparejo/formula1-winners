// @flow
import api from 'api';

function getStandingsBySeason(season: string): Promise<*> {
  return api.get(`/${season}/driverStandings.json`);
}

export default {
  getStandingsBySeason
};
