// @flow
import { connect } from 'react-redux';
import DriverFavoriteIcon from './DriverFavoriteIcon';
import { selectors } from '../../duck';
import {
  addDriverToFavoritesStorage,
  removeDriverToFavoritesStorage
} from '../../asyncActions';

type MapDispatchToProps = {
  addToFavorites: Function,
  removeFromFavorites: Function
};

type MapStateToProps = {
  isFavorite: boolean
};

type OwnProps = {
  driverId: string
};

export function mapDispatchToProps(
  dispatch: Function,
  { driverId }: OwnProps
): MapDispatchToProps {
  return {
    addToFavorites: () => dispatch(addDriverToFavoritesStorage(driverId)),
    removeFromFavorites: () =>
      dispatch(removeDriverToFavoritesStorage(driverId))
  };
}

export function mapStateToProps(
  state: Object,
  { driverId }: OwnProps
): MapStateToProps {
  return {
    isFavorite: selectors.selectIsFavoriteDriver(state, driverId)
  };
}

const DriverFavoriteIconContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(DriverFavoriteIcon);

DriverFavoriteIconContainer.displayName = 'DriverFavoriteIconContainer';

export default DriverFavoriteIconContainer;
