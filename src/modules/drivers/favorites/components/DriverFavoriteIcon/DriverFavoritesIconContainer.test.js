import { MODULE_NAME } from '../../../constants';
import { selectors, FAVORITES_STATE_NAME, initialState } from '../../duck';
import * as asyncActions from '../../asyncActions';
import {
  mapDispatchToProps,
  mapStateToProps
} from './DriverFavoriteIconContainer';

describe('<DriverFavoriteIconContainer />', () => {
  describe('mapStateToProps', () => {
    test('should call selectIsFavoriteDriver selectors', () => {
      const mocks = {
        ownProps: { driverId: '1' },
        state: {
          [MODULE_NAME]: {
            [FAVORITES_STATE_NAME]: {
              ...initialState,
              fetchedFromStorage: true
            }
          }
        }
      };
      const spy = jest.spyOn(selectors, 'selectIsFavoriteDriver');

      mapStateToProps(mocks.state, mocks.ownProps);

      expect(spy).toHaveBeenCalledTimes(1);
    });
  });

  describe('mapDispatchToProps', () => {
    test('should addToFavorites call to addDriverToFavoritesStorage action', () => {
      const mocks = {
        ownProps: { driverId: '1' },
        dispatch: jest.fn()
      };
      const props = mapDispatchToProps(mocks.dispatch, mocks.ownProps);
      const spy = jest.spyOn(asyncActions, 'addDriverToFavoritesStorage');
      const spyDispatch = jest.spyOn(mocks, 'dispatch');

      props.addToFavorites();

      expect(spyDispatch).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mocks.ownProps.driverId);
    });

    test('should removeFromFavorites call to removeDriverToFavoritesStorage action', () => {
      const mocks = {
        ownProps: { driverId: '1' },
        dispatch: jest.fn()
      };
      const props = mapDispatchToProps(mocks.dispatch, mocks.ownProps);
      const spy = jest.spyOn(asyncActions, 'removeDriverToFavoritesStorage');
      const spyDispatch = jest.spyOn(mocks, 'dispatch');

      props.removeFromFavorites();

      expect(spyDispatch).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(mocks.ownProps.driverId);
    });
  });
});
