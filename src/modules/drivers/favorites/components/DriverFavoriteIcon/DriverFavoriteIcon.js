// @flow
import React from 'react';
import type { Node } from 'react';
import { FavoriteIcon } from 'components/Icons';
import { Wrapper } from './styles';

type Props = {
  isFavorite: boolean,
  addToFavorites: Function,
  removeFromFavorites: Function
};

function DriverFavoriteIcon({
  isFavorite,
  addToFavorites,
  removeFromFavorites
}: Props): Node {
  return (
    <Wrapper
      onClick={isFavorite ? removeFromFavorites : addToFavorites}
      isFavorite={isFavorite}
    >
      <FavoriteIcon height={20} width={20} />
    </Wrapper>
  );
}

export default DriverFavoriteIcon;
