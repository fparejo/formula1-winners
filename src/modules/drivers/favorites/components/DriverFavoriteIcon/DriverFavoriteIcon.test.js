import React from 'react';
import { shallow } from 'enzyme';
import DriverFavoriteIcon from './DriverFavoriteIcon';

const mockProps = {
  isFavorite: false,
  addToFavorites: jest.fn(),
  removeFromFavorites: jest.fn()
};

describe('<DriverFavoriteIcon />', () => {
  test('should render the component', () => {
    const wrapper = shallow(<DriverFavoriteIcon {...mockProps} />);
    expect(wrapper.exists()).toBeTruthy();
  });

  test('should has isFavorite right value', () => {
    const props = { ...mockProps, isFavorite: true };
    const wrapper = shallow(<DriverFavoriteIcon {...props} />);
    expect(wrapper.props().isFavorite).toBe(true);
  });

  test('should call addToFavorites method when isFavorite equals false', () => {
    const props = { ...mockProps, isFavorite: false };
    const wrapper = shallow(<DriverFavoriteIcon {...props} />);
    const spy = jest.spyOn(mockProps, 'addToFavorites');

    wrapper.simulate('click');
    expect(spy).toHaveBeenCalledTimes(1);
  });

  test('should call removeFromFavorites method when isFavorite equals true', () => {
    const props = { ...mockProps, isFavorite: true };
    const wrapper = shallow(<DriverFavoriteIcon {...props} />);
    const spy = jest.spyOn(mockProps, 'removeFromFavorites');

    wrapper.simulate('click');
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
