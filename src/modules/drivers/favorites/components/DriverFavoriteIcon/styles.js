import styled from 'styled-components';

export const Wrapper = styled.div`
  svg {
    cursor: pointer;
    fill: ${props =>
      props.isFavorite ? props.theme.colors.red : props.theme.colors.black};
  }
`;
