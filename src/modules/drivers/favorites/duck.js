// @flow
import { MODULE_NAME } from '../constants';

type State = {
  data: Array<string>,
  fetchedFromStorage: boolean
};

export const FAVORITES_STATE_NAME = 'favorites';

/* ACTIONS */
const actionsTypes = {
  SET_FAVORITE_DRIVERS: 'SET_FAVORITE_DRIVERS',
  SET_FAVORITES_FETCHED_FROM_STORAGE: 'SET_FAVORITES_FETCHED_FROM_STORAGE',
  ADD_DRIVER_TO_FAVORITES: 'ADD_DRIVER_TO_FAVORITE',
  REMOVE_DRIVER_FROM_FAVORITES: 'REMOVE_DRIVER_FROM_FAVORITES'
};

export const actions = {
  setFavoriteDrivers: (driversIds: Array<string>) => ({
    type: actionsTypes.SET_FAVORITE_DRIVERS,
    payload: driversIds
  }),
  setFavoritesFetchedFromStorage: () => ({
    type: actionsTypes.SET_FAVORITES_FETCHED_FROM_STORAGE
  }),
  addDriveToFavorites: (driverId: string) => ({
    type: actionsTypes.ADD_DRIVER_TO_FAVORITES,
    payload: driverId
  }),
  removeDriveFromFavorites: (driverIndex: number) => ({
    type: actionsTypes.REMOVE_DRIVER_FROM_FAVORITES,
    payload: driverIndex
  })
};

/* REDUCER */
export const initialState: State = {
  data: [],
  fetchedFromStorage: false
};

export default function reducer(
  state: State = initialState,
  action: Action
): State {
  switch (action.type) {
    case actionsTypes.SET_FAVORITE_DRIVERS:
      return { ...state, data: action.payload };
    case actionsTypes.SET_FAVORITES_FETCHED_FROM_STORAGE:
      return { ...state, fetchedFromStorage: true };
    case actionsTypes.ADD_DRIVER_TO_FAVORITES:
      return { ...state, data: [...state.data, action.payload] };
    case actionsTypes.REMOVE_DRIVER_FROM_FAVORITES:
      return {
        ...state,
        data: [
          ...state.data.slice(0, action.payload),
          ...state.data.slice(action.payload + 1)
        ]
      };
    default:
      return state;
  }
}

/* SELECTORS */
export const selectors = {
  selectFavorites: (state: Object): Array<string> =>
    state[MODULE_NAME][FAVORITES_STATE_NAME].data,
  selectIsFavoriteDriver: (state: Object, driverId: string): boolean =>
    state[MODULE_NAME][FAVORITES_STATE_NAME].data.includes(driverId),
  selectFavoriteDriverIndex: (state: Object, driverId: string): number =>
    state[MODULE_NAME][FAVORITES_STATE_NAME].data.findIndex(
      favoriteDriverId => favoriteDriverId === driverId
    ),
  selectWasFetchedFromStorage: (state: Object) =>
    state[MODULE_NAME][FAVORITES_STATE_NAME].fetchedFromStorage
};
