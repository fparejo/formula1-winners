import { MODULE_NAME } from '../constants';
import reducer, {
  actions,
  initialState,
  selectors,
  FAVORITES_STATE_NAME
} from './duck';

describe('Drivers favorites duck', () => {
  describe('Actions', () => {
    test('should setFavoriteDrivers return an action', () => {
      const action = actions.setFavoriteDrivers('2');
      expect(action).toMatchSnapshot();
    });

    test('should setFavoritesFetchedFromStorage return an action', () => {
      const action = actions.setFavoriteDrivers();
      expect(action).toMatchSnapshot();
    });

    test('should addDriveToFavorites return an action', () => {
      const action = actions.addDriveToFavorites('1');
      expect(action).toMatchSnapshot();
    });

    test('should removeDriveFromFavorites return an action', () => {
      const action = actions.removeDriveFromFavorites('1');
      expect(action).toMatchSnapshot();
    });
  });

  describe('Reducer', () => {
    test('should SET_FAVORITE_DRIVERS return a new state', () => {
      const payload = ['1', '2', '3'];
      const action = actions.setFavoriteDrivers(payload);
      const mockState = reducer(initialState, action);

      expect(mockState).toEqual({ ...initialState, data: payload });
    });

    test('should SET_FAVORITES_FETCHED_FROM_STORAGE return a new state', () => {
      const action = actions.setFavoritesFetchedFromStorage();
      const mockState = reducer(initialState, action);

      expect(mockState).toEqual({ ...initialState, fetchedFromStorage: true });
    });

    test('should ADD_DRIVER_TO_FAVORITES return a new state', () => {
      const action = actions.addDriveToFavorites('1');
      const mockState = reducer(initialState, action);

      expect(mockState).toEqual({ ...initialState, data: ['1'] });
    });

    test('should REMOVE_DRIVER_FROM_FAVORITES return a new state', () => {
      const action = actions.removeDriveFromFavorites(1);
      const mockState = reducer(
        { ...initialState, data: ['1', '2', '3'] },
        action
      );

      expect(mockState).toEqual({ ...initialState, data: ['1', '3'] });
    });
  });

  describe('Selectors', () => {
    test('should selectFavorites return a favorites list', () => {
      const data = ['1', '2', '3'];
      const mockState = {
        [MODULE_NAME]: {
          [FAVORITES_STATE_NAME]: { ...initialState, data }
        }
      };
      const result = selectors.selectFavorites(mockState);

      expect(result).toEqual(data);
    });

    test('should selectIsFavoriteDriver return true when driverId is included in data array', () => {
      const data = ['1', '2', '3'];
      const mockState = {
        [MODULE_NAME]: {
          [FAVORITES_STATE_NAME]: { ...initialState, data }
        }
      };
      const result = selectors.selectIsFavoriteDriver(mockState, '2');

      expect(result).toBeTruthy();
    });

    test('should selectIsFavoriteDriver return false when driverId is not included in data array', () => {
      const data = ['1', '2', '3'];
      const mockState = {
        [MODULE_NAME]: {
          [FAVORITES_STATE_NAME]: { ...initialState, data }
        }
      };
      const result = selectors.selectIsFavoriteDriver(mockState, '5');

      expect(result).toBeFalsy();
    });

    test('should selectFavoriteDriverIndex return driver index', () => {
      const data = ['1', '2', '3'];
      const mockState = {
        [MODULE_NAME]: {
          [FAVORITES_STATE_NAME]: { ...initialState, data }
        }
      };
      const result = selectors.selectFavoriteDriverIndex(mockState, '2');

      expect(result).toBe(1);
    });

    test('should selectWasFetchedFromStorage return fetchedFromStorage value', () => {
      const mockState = {
        [MODULE_NAME]: {
          [FAVORITES_STATE_NAME]: { ...initialState, fetchedFromStorage: true }
        }
      };
      const result = selectors.selectWasFetchedFromStorage(mockState);

      expect(result).toBe(true);
    });
  });
});
