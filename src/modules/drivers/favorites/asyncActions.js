// @flow
import { LOCALSTORAGE_KEYS } from '../constants';
import { actions, selectors } from './duck';

export const fecthFavoriteDriversStorage = () => (dispatch: Function) => {
  const favoritesFromStorage = window.localStorage.getItem(
    LOCALSTORAGE_KEYS.favorites
  );

  if (favoritesFromStorage) {
    dispatch(actions.setFavoritesFetchedFromStorage());
    dispatch(actions.setFavoriteDrivers(JSON.parse(favoritesFromStorage)));
  }
};

export const addDriverToFavoritesStorage = (driverId: string) => (
  dispatch: Function,
  getState: Function
) => {
  dispatch(actions.addDriveToFavorites(driverId));

  const favorites = selectors.selectFavorites(getState());
  window.localStorage.setItem(
    LOCALSTORAGE_KEYS.favorites,
    JSON.stringify(favorites)
  );
};

export const removeDriverToFavoritesStorage = (driverId: string) => (
  dispatch: Function,
  getState: Function
) => {
  const driverToRemoveIndex = selectors.selectFavoriteDriverIndex(
    getState(),
    driverId
  );
  dispatch(actions.removeDriveFromFavorites(driverToRemoveIndex));

  const favorites = selectors.selectFavorites(getState());
  window.localStorage.setItem(
    LOCALSTORAGE_KEYS.favorites,
    JSON.stringify(favorites)
  );
};
