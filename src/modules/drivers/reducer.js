// @flow
import { combineReducers } from 'redux';
import standingsReducer, { STANDINGS_STATE_NAME } from './standings/duck';
import listReducer, { LIST_STATE_NAME } from './list/duck';
import favoritesReducer, { FAVORITES_STATE_NAME } from './favorites/duck';

export default combineReducers({
  [STANDINGS_STATE_NAME]: standingsReducer,
  [LIST_STATE_NAME]: listReducer,
  [FAVORITES_STATE_NAME]: favoritesReducer
});
