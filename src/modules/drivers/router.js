// @flow
import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import { DriverStandingListView } from './standings/views';
import { ROUTES } from './constants';

function DriversRouter({ match }: Route): React.Node {
  return (
    <Switch>
      <Route
        exact
        path={`${match.path}${ROUTES.paths.driversStandingsList.path}`}
        component={DriverStandingListView}
      />
    </Switch>
  );
}

export default DriversRouter;
