// @flow
import { normalize, schema } from 'normalizr';
import type { DriverStandingAPI, DriverAPI } from './types';

const driverSchema = new schema.Entity(
  'drivers',
  {},
  { idAttribute: 'driverId' }
);
const driversSchema = new schema.Array(driverSchema);
const constructorSchema = new schema.Entity(
  'constructors',
  {},
  { idAttribute: 'name' }
);
const constructorsSchema = new schema.Array(constructorSchema);

const driversStandingsSchema = new schema.Array({
  Driver: driverSchema,
  Constructors: constructorsSchema
});

export default {
  driversStandings: (apiData: DriverStandingAPI) =>
    normalize(apiData, driversStandingsSchema),
  drivers: (apiData: DriverAPI) => normalize(apiData, driversSchema)
};
