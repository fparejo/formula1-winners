// @flow
export type Driver = {
  driverId: string,
  familyName: string,
  givenName: string,
  dateOfBirth: string,
  nationality: string
};

export type DriverStanding = {
  Driver: string,
  Constructors: Array<string>,
  points: string,
  wins: string,
  position: string
};

export type DriverAPI = {
  code: string,
  dateOfBirth: string,
  driverId: string,
  familyName: string,
  givenName: string,
  nationality: string,
  permanentNumber: string,
  url: string
};

export type DriverStandingAPI = {
  Driver: DriverAPI,
  points: string,
  position: string,
  positionText: string,
  wins: string
};
