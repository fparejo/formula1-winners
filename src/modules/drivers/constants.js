export const MODULE_NAME = 'drivers';

export const ROUTE_PATH = '/drivers';

export const ROUTES = {
  paths: {
    driversStandingsList: {
      path: '/standings/:season',
      staticPath: season => `${ROUTE_PATH}/standings/${season}`
    }
  }
};

export const LOCALSTORAGE_KEYS = {
  favorites: 'favorites'
};
