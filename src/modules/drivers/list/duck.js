// @flow
import { MODULE_NAME } from '../constants';
import type { Driver } from '../types';

type State = {
  data: { [string]: Driver }
};

export const LIST_STATE_NAME = 'list';

/* ACTIONS */
const actionsTypes = {
  UPDATE_DRIVERS_DATA: 'UPDATE_DRIVERS_DATA'
};

export const actions = {
  updateDriversData: (drivers: { [string]: Driver }): Action => ({
    type: actionsTypes.UPDATE_DRIVERS_DATA,
    payload: drivers
  })
};

/* REDUCER */
export const initialState: State = {
  data: {}
};

export default function reducer(
  state: State = initialState,
  action: Action
): State {
  switch (action.type) {
    case actionsTypes.UPDATE_DRIVERS_DATA:
      return { ...state, data: { ...state.data, ...action.payload } };
    default:
      return state;
  }
}

/* SELECTORS */
export const selectors = {
  selectDrivers: (state: Object) => state[MODULE_NAME][LIST_STATE_NAME].data
};
