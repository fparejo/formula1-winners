import { MODULE_NAME } from '../constants';
import reducer, {
  actions,
  initialState,
  selectors,
  LIST_STATE_NAME
} from './duck';

describe('Drivers list duck', () => {
  describe('Actions', () => {
    test('should updateDriversData return an action', () => {
      const action = actions.updateDriversData(['1', '2']);
      expect(action).toMatchSnapshot();
    });
  });

  describe('Reducer', () => {
    test('should UPDATE_DRIVERS_DATA return a new state', () => {
      const payload = { '1': { id: '1' }, '2': { id: '2' } };
      const action = actions.updateDriversData(payload);
      const mockState = reducer(initialState, action);

      expect(mockState).toEqual({ ...initialState, data: payload });
    });
  });

  describe('Selectors', () => {
    test('should selectDrivers return a drivers list', () => {
      const data = { '1': { id: '1' }, '2': { id: '2' } };
      const mockState = {
        [MODULE_NAME]: {
          [LIST_STATE_NAME]: { ...initialState, data }
        }
      };
      const result = selectors.selectDrivers(mockState);

      expect(result).toEqual(data);
    });
  });
});
