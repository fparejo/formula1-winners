// @flow
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { AppRouter } from './modules/app';
import store from './store';

render(
  <Provider store={store}>
    <BrowserRouter>
      <Route path="/" component={AppRouter} />
    </BrowserRouter>
  </Provider>,
  document.getElementById('app') || document.createElement('div')
);
