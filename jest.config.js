module.exports = {
  moduleFileExtensions: ['js'],
  moduleDirectories: ['node_modules', 'src'],
  setupTestFrameworkScriptFile: './src/setupTests',
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
    '^.+\\.svg$': 'jest-svg-transformer'
  }
};
