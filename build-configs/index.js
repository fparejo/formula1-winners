const development = require('./webpack.development');
const production = require('./webpack.production');

module.exports = {
  development,
  production
};
