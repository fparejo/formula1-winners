declare type Action = Object;

declare type Season = {
  season: string,
  url: string
};
