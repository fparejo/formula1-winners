const path = require('path');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const configs = require('./build-configs');

module.exports = env => {
  const mode = env ? env.NODE_ENV : 'development';

  return merge(
    {
      mode,
      entry: './src/index.js',
      output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/'
      },
      resolve: {
        extensions: ['.js'],
        modules: [path.resolve('./src'), 'node_modules']
      },
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: 'babel-loader'
          },
          {
            test: /\.svg$/,
            use: [
              {
                loader: 'babel-loader'
              },
              {
                loader: 'react-svg-loader',
                options: {
                  jsx: true
                }
              }
            ]
          }
        ]
      },
      plugins: [
        new HtmlWebpackPlugin({
          template: './src/index.html'
        })
      ]
    },
    configs[mode]
  );
};
